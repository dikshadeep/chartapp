import React, {Component} from 'react';
import { 
	View, 
	Text, 
	StyleSheet, 
	Animated, 
	PanResponder, 
	Dimensions, 
	TouchableOpacity, 
	TouchableWithoutFeedback,
	Platform
} from 'react-native';
import {
  LineChart,
  BarChart,
  PieChart,
  ProgressChart,
  ContributionGraph
} from '../react-native-chart-kit'

const screenWidth = Dimensions.get('window').width
const chartConfig = {
  backgroundGradientFrom: '#1E2923',
  backgroundGradientTo: '#08130D',
  color: (opacity = 1) => `rgba(26, 255, 146, ${opacity})`,
  strokeWidth: 2 // optional, default 3
}

export default class MyApp extends Component {
	constructor(props) {
		super(props);
		this.state = {
			data : [
			  { name: 'C1', population: 21500000, color: '#F5C25D', legendFontColor: '#7F7F7F', legendFontSize: 15 },
			  { name: 'C2', population: 21500000, color: 'orange', legendFontColor: '#7F7F7F', legendFontSize: 15 },
			  { name: 'C3', population: 21500000, color: 'black', legendFontColor: '#7F7F7F', legendFontSize: 15 },
			  { name: 'C4', population: 21500000, color: 'brown', legendFontColor: '#7F7F7F', legendFontSize: 15 },
			  { name: 'C5', population: 21500000, color: 'rgba(131, 167, 234, 1)', legendFontColor: '#7F7F7F', legendFontSize: 15 },
			  { name: 'C6', population: 21500000, color: 'red', legendFontColor: '#7F7F7F', legendFontSize: 15 },
			  { name: 'C7', population: 21500000, color: '#ffffff', legendFontColor: '#7F7F7F', legendFontSize: 15 },
			  { name: 'C8', population: 21500000, color: 'rgb(0, 0, 255)', legendFontColor: '#7F7F7F', legendFontSize: 15 }
			],
			showDraggable: true,
			dropZoneValues: {},
			pan     : {
				firstBall: new Animated.ValueXY(),
				secondBall: new Animated.ValueXY(),
				thirdBall: new Animated.ValueXY(),
				forthBall: new Animated.ValueXY()
			}
		}
		this.dropArea = [];
		this.panResponder = {};
		Object.keys(this.state.pan).map((key) => {
			this.panResponder[key] = PanResponder.create({    
	        onStartShouldSetPanResponder : () => true,
	        onPanResponderMove           : Animated.event([null,{ 
	            dx : this.state.pan[key].x,
	            dy : this.state.pan[key].y
	        }]),
	        onPanResponderRelease : (e, gesture) => {		
	        				// console.log('e--- Y', e	)
	        				// console.warn('heerererer')
						this.setState({showDraggable: false});
						this.isInsideSector({ x: gesture.moveX-13, y: gesture.moveY-188}, {x:88,y:102}, {x:167.5, y:102}, {x:145, y:46}, 79.5)

					if(this.isDropZone(gesture, key)) {

					}else {
							Animated.spring(
							this.state.pan[key],
							{toValue: {x:0, y:0}}
						).start();
					}
				}
			})			
		});
		setTimeout(() => {
			console.log('this.dropArea: ', this.dropArea);
		}, 5000)
	}

	setDropZoneValues = (key, event) => {
		// console.log('event-- ', event.nativeEvent.layout)
		let dropZoneValues = [ ...this.state.dropZoneValues ];
		dropZoneValues[key] = event.nativeEvent.layout;
		this.setState({ dropZoneValues })
	}

	isDropZone(gesture, key) {
		// console.warn('gesture ', gesture)
		// this.state.dropZoneValues.map((dz, i) => {
		// 	console.log('dzone ')
		// 	// return gesture.moveY> dz.y && gesture.moveY < dz.y + dz.height;	
		// })
		// return gesture.moveY> dz.y && gesture.moveY < dz.y + dz.height;
	}
    isInsideSector(point, center, sectorStart, sectorEnd, radiusSquared) {
        var relPoint = {
          x: point.x - center.x,
          y: point.y - center.y
        };

        // console.warn("ressulttttt-- ", !this.areClockwise(sectorStart, relPoint) && this.areClockwise(sectorEnd, relPoint) && this.isWithinRadius(relPoint, radiusSquared));
      }

    areClockwise(v1, v2) {
    	// console.warn('areClockwise-- ', -v1.x*v2.y + v1.y*v2.x > 0)
        return -v1.x*v2.y + v1.y*v2.x > 0;
      }

    isWithinRadius(v, radiusSquared) {
        return v.x*v.x + v.y*v.y <= radiusSquared;
      }

	render() {
		return(
				<View style={{flex:1}} >
					<View style={{ flex: 0.7,justifyContent: 'center',  alignItems: 'center', flexDirection:'row'}}>
						
						<TouchableWithoutFeedback onPressIn={(e)=> {
							// console.warn('datax', e.nativeEvent.locationX);
							// console.warn('data', e.nativeEvent.locationY);
						}}>
							<PieChart
							  data={this.state.data}
							  width={screenWidth}
							  height={250}
							  chartConfig={chartConfig}
							  accessor="population"
							  backgroundColor="transparent"
							  paddingLeft={30}
							/>
						</TouchableWithoutFeedback>
						
					</View>
					<View style={{ flexDirection:'row',flex: 0.2, justifyContent: 'center'}}>
						<View style={{ alignItems: 'center',flexDirection: 'row'}} >
							<View style={[styles.draggableContainer]}>
								<Animated.View 
								{...this.panResponder.firstBall.panHandlers}
								style={[this.state.pan.firstBall.getLayout(), styles.circle, {backgroundColor: '#1ED2FA'}]} />
							</View>
							
							<View style={{ margin: 10}}>
								<Animated.View 
								{...this.panResponder.secondBall.panHandlers}
								style={[this.state.pan.secondBall.getLayout(), styles.circle, {backgroundColor: '#4D91FD'}]} />
							</View>
							<View style={{ margin: 10}}>
								<Animated.View 
								{...this.panResponder.thirdBall.panHandlers}
								style={[this.state.pan.thirdBall.getLayout(), styles.circle, {backgroundColor: '#4FC989'}]} />
							</View>
							<View style={{ margin: 10}}>
								<Animated.View 
								{...this.panResponder.forthBall.panHandlers}
								style={[this.state.pan.forthBall.getLayout(), styles.circle, {backgroundColor: '#F5C25D'}]} />
							</View>
						</View>
						
					</View>
					<View style={{flexDirection: 'row', bottom: 10, position: 'absolute', bottom: 5, flex:0.1 }}>
						<View style={{alignItems: 'center', flex: 0.5}}>
							<TouchableOpacity >
								<Text style={{fontSize: 20, padding: 20}} >Undo</Text>
							</TouchableOpacity>
						</View>
						<View style={{alignItems: 'center', flex: 0.5}}>
							<TouchableOpacity >
								<Text style={{fontSize: 20, padding: 20}} >Revert</Text>
							</TouchableOpacity>
						</View>
					</View>
				</View>
			)
	}
}

let CIRCLE_RADIUS = 20;
const styles = StyleSheet.create({
    draggableContainer: {
    	margin: 10
        // position    : 'absolute',
        // top         : Window.height/2 - CIRCLE_RADIUS,
        // left        : Window.width/2 - CIRCLE_RADIUS,
    },
    circle      : {
        backgroundColor     : 'red',
        width               : CIRCLE_RADIUS*2,
        height              : CIRCLE_RADIUS*2,
        borderRadius        : CIRCLE_RADIUS
    }
})